#!/usr/bin/env bash

## @NOTE
## ASDK_EXTRA_LANGS is deprecated, but still supported
if [[ -n "$ASDK_EXTRA_LOCALES" ]]; then
    locales="$ASDK_EXTRA_LOCALES"
elif [[ -n "$ASDK_EXTRA_LANGS" ]]; then
    echo "ASDK_EXTRA_LANGS environment variable is deprecated. Please use ASDK_EXTRA_LOCALES"
    locales="$ASDK_EXTRA_LANGS"
fi

if [[ -n "$locales" ]]; then
    echo "${locales//,/ }" | xargs locale-gen
fi

if [[ "$ASDK_PROMPT_LICENSES" == "yes" || "$ASDK_PROMPT_LICENSES" == "true" || "$ASDK_PROMPT_LICENSES" == "1" ]]; then
    prompt_licenses=1
    if [[ "$ASDK_ACCEPT_LICENSES" == "yes" || "$ASDK_ACCEPT_LICENSES" == "true" || "$ASDK_ACCEPT_LICENSES" == "1" ]]; then
        accept=1
        if [[ "$ASDK_ACCEPT_LICENSES_SILENT" == "yes" || "$ASDK_ACCEPT_LICENSES_SILENT" == "true" || "$ASDK_ACCEPT_LICENSES_SILENT" == "1" ]]; then
            silent=1
        fi
    fi
fi

if [[ "$prompt_licenses" == "1" ]]; then
    if [[ "$accept" == "1" ]]; then
        if [[ "$silent" == "1" ]]; then
            accept-licenses.exp > /dev/null
        else
            accept-licenses.exp
        fi
    else
        sdkmanager --licenses
    fi
fi

exec "$@"
