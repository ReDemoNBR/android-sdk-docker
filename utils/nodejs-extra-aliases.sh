#!/usr/bin/env bash

set -e

end=""

[[ "$BUILD_VARIANT" == "alpine" ]] && end="-alpine"

extra_prefix_aliases=()

for version in "$@"; do
    extra_prefix_aliases+=("node-${version}$end")
done

output="$(printf ",%s" "${extra_prefix_aliases[@]}")"

echo "${output:1}"
